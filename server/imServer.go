package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

func main() {

	fmt.Println("Launching Server...")

	// listen on all interfaces
	ln, _ := net.Listen("tcp", ":8000")

	// accept connection on port
	conn, _ := ln.Accept()

	// infinite loop (until ctrl-c)
	for {
		// listen for message to process ending in newline
		message, _ := bufio.NewReader(conn).ReadString('\n')

		// output message received
		fmt.Print("Message Received: ", string(message))

		// sample process for string received
		newMessage := strings.ToUpper(message)

		// send new string back to client
		conn.Write([]byte(newMessage + "\n"))
	}
}
